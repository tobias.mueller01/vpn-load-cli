# VPN Load CLI

Display the current load of GWDG VPNs in your terminal.

## Installation

Make sure you have Python 3.6+ and [`requests`](https://requests.readthedocs.io/en/master/) installed.

## Usage

```
$ python3 vpnload.py
```

## Output

![img.png](screenshot.png)