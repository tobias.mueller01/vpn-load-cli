#!/usr/bin/env python3

from enum import Enum
from typing import Dict

import requests

# GWDG-side config
SERVERS = ["vpn", "vpn-b", "vpn-c"]
MAX_CONNECTIONS = 750

# Display config
BAR_LENGTH = 30         # length of progress bar in number of characters
BAR_CHAR = "#"          # character used to fill the bar
WARNING_THRESHOLD = 85  # percentage above which to warn


class Color(str, Enum):
    """ANSI escape strings to color terminal output"""
    RED = "\033[91m"
    GREEN = "\033[92m"
    ENDC = "\033[0m"


def get_data_for_server(server: str) -> Dict:
    url = f"https://status.gwdg.de/vpn/data/{server}.json"
    # Ignore certificate verification because it doesn't work with the https://status.gwdg.de subdomain
    response = requests.get(url, verify=False)
    return response.json()


def calculate_percentage(server_data: Dict) -> int:
    # see https://status.gwdg.de/vpn/scripts.js
    percent = round((server_data["results"][0]["series"][0]["values"][0][1] / MAX_CONNECTIONS) * 100)
    return percent


def color_string(text: str, color: Color) -> str:
    return color + text + Color.ENDC


def get_progress_bar_string(current: int, total: int) -> str:
    n = int(current / total * BAR_LENGTH)

    if current/total * 100 < WARNING_THRESHOLD:
        color = Color.GREEN
    else:
        color = Color.RED
    return f"[{color_string(n * BAR_CHAR + (BAR_LENGTH - n) * ' ', color)}]"


def display_progress_bar(server_name: str, load: int):
    if load < WARNING_THRESHOLD:
        color = Color.GREEN
    else:
        color = Color.RED
    # Pad the string width to 14 character: 2 * color escape string + 3 digit number + space + %
    print(f"{server_name}:\t{get_progress_bar_string(load, 100)} {color_string(str(load) + ' %', color).rjust(14, ' ')}")


def main():
    # Suppress warning because we don't verify the SSL certificate
    import warnings
    warnings.filterwarnings('ignore')

    load = {}

    for server in SERVERS:
        server_data = get_data_for_server(server)
        percentage = calculate_percentage(server_data)
        load[server] = percentage

    for entry in load:
        display_progress_bar(entry, load[entry])


if __name__ == "__main__":
    main()
